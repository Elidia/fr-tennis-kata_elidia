﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace tennis_kata_test
{
      public class Players {

        public string Name { get; set; }
        public int Points { get; set; }

        // Constructor
        public Players(string name, int points)
        {
            Name = name;
            Points = points;
         }

    }
        
    [TestClass]
    public class tennis_kata_test
    {
        private Players Player1 = new Players("Peppa Pig", 0);
        private Players Player2 = new Players("Danny Dog", 0);

        [TestMethod]
        public void Add_ShouldReturnLove_WhenPlayersZeroZero()
        {
            Player1.Points = 0;
            Player2.Points = 0;
            Assert.AreEqual("Love", Score.Add(Player1, Player2));
        }

        [TestMethod]
        public void Add_ShouldReturnDifferentPoints_WhenPlayersNotDeuce()
        {
            Player2.Points = 0;

            Player1.Points = 1;            
            Assert.AreEqual("Fifteen : Love", Score.Add(Player1, Player2));
            Player1.Points = 2;
            Assert.AreEqual("Thirty : Love", Score.Add(Player1, Player2));
            Player1.Points = 3;
            Assert.AreEqual("Forty : Love", Score.Add(Player1, Player2));

            Player2.Points = 1;

            Player1.Points = 0;            
            Assert.AreEqual("Love : Fifteen", Score.Add(Player1, Player2));
            Player1.Points = 1;
            Assert.AreEqual("Fifteen : Fifteen", Score.Add(Player1, Player2));
            Player1.Points = 2;
            Assert.AreEqual("Thirty : Fifteen", Score.Add(Player1, Player2));
            Player1.Points = 3;
            Assert.AreEqual("Forty : Fifteen", Score.Add(Player1, Player2));

            Player2.Points = 2;

            Player1.Points = 0;            
            Assert.AreEqual("Love : Thirty", Score.Add(Player1, Player2));
            Player1.Points = 1;
            Assert.AreEqual("Fifteen : Thirty", Score.Add(Player1, Player2));
            Player1.Points = 2;
            Assert.AreEqual("Thirty : Thirty", Score.Add(Player1, Player2));
            Player1.Points = 3;
            Assert.AreEqual("Forty : Thirty", Score.Add(Player1, Player2));
                        
            Player2.Points = 3;

            Player1.Points = 0;
            Assert.AreEqual("Love : Forty", Score.Add(Player1, Player2));
            Player1.Points = 1;
            Assert.AreEqual("Fifteen : Forty", Score.Add(Player1, Player2));
            Player1.Points = 2;
            Assert.AreEqual("Thirty : Forty", Score.Add(Player1, Player2));
        }

        [TestMethod]
        public void Add_ShouldReturnDeuce_WhenPlayersEqualThreePoints()
        {
            Player1.Points = 3;
            Player2.Points = 3;
            Assert.AreEqual("Deuce", Score.Add( Player1 , Player2));
        }

        [TestMethod]
        public void Add_ShouldReturnAdvantatge_WhenOnePlayerEqualFourPoints()
        {
            Player1.Points = 4;
            Player2.Points = 0;
            Assert.AreEqual("Advantage " + Player1.Name, Score.Add(Player1, Player2));
            Player2.Points = 1;
            Assert.AreEqual("Advantage " + Player1.Name, Score.Add(Player1, Player2));
            Player2.Points = 2;
            Assert.AreEqual("Advantage " + Player1.Name, Score.Add(Player1, Player2));
            Player2.Points = 3;
            Assert.AreEqual("Advantage " + Player1.Name, Score.Add(Player1, Player2));

            Player1.Points = 0;
            Player2.Points = 4;
            Assert.AreEqual("Advantage " + Player2.Name, Score.Add(Player1, Player2));
            Player1.Points = 1;
            Assert.AreEqual("Advantage " + Player2.Name, Score.Add(Player1, Player2));
            Player1.Points = 2;
            Assert.AreEqual("Advantage " + Player2.Name, Score.Add(Player1, Player2));
            Player1.Points = 3;
            Assert.AreEqual("Advantage " + Player2.Name, Score.Add(Player1, Player2));
        }

        [TestMethod]
        public void Add_ShouldReturnGame_WhenOnePlayerEqualFivePoints()
        {
            Player1.Points = 5;
            Player2.Points = 0;
            Assert.AreEqual("Game " + Player1.Name, Score.Add(Player1, Player2));
            Player2.Points = 1;
            Assert.AreEqual("Game " + Player1.Name, Score.Add(Player1, Player2));
            Player2.Points = 2;
            Assert.AreEqual("Game " + Player1.Name, Score.Add(Player1, Player2));
            Player2.Points = 3;
            Assert.AreEqual("Game " + Player1.Name, Score.Add(Player1, Player2));
            Player2.Points = 4;
            Assert.AreEqual("Game " + Player1.Name, Score.Add(Player1, Player2));

            Player1.Points = 0;
            Player2.Points = 5;
            Assert.AreEqual("Game " + Player2.Name, Score.Add(Player1, Player2));
            Player1.Points = 1;
            Assert.AreEqual("Game " + Player2.Name, Score.Add(Player1, Player2));
            Player1.Points = 2;
            Assert.AreEqual("Game " + Player2.Name, Score.Add(Player1, Player2));
            Player1.Points = 3;
            Assert.AreEqual("Game " + Player2.Name, Score.Add(Player1, Player2));
            Player1.Points = 4;
            Assert.AreEqual("Game " + Player2.Name, Score.Add(Player1, Player2));
        }

    }

    internal class Score
    {
        internal static string Add(Players player1, Players player2)
        {
            string score = string.Empty;

            if (isLove(player1.Points, player2.Points)) { score = GetValuePoint(0); }

            if (isDeuce(player1.Points, player2.Points)) { score = GetValuePoint(-1); }

            if (isDifferentPoints(player1.Points, player2.Points)) { score = GetValuePoint(player1.Points) + " : " + GetValuePoint(player2.Points); }
            
            if ((isAdvantagePlayer1(player1.Points, player2.Points)) || (isGamePlayer1(player1.Points, player2.Points))) { score = GetValuePoint(player1.Points) + " " + player1.Name; }

            if ((isAdvantagePlayer2(player1.Points, player2.Points)) || (isGamePlayer2(player1.Points, player2.Points))) { score = GetValuePoint(player2.Points) + " " + player2.Name; }

            return score;

        }

        private static Boolean isLove(int player1, int player2)
        {
            return ((player1 == 0) && (player2 == 0));
        }
        
        private static Boolean isDeuce(int player1, int player2)
        {
            return ((player1 == 3) && (player2 == 3));
        }

        private static Boolean isDifferentPoints(int player1, int player2) {
            return ( ((player1 < 4) && (player2 < 4)) && ((player1 != 3) | (player2 != 3)) && ((player1 != 0) | (player2 != 0)) );
        }

        private static Boolean isAdvantagePlayer1(int player1, int player2)
        {
            return ((player1 == 4) && (player2 < 4));
        }

        private static Boolean isAdvantagePlayer2(int player1, int player2)
        {
            return ((player1 < 4) && (player2 == 4));
        }

        private static Boolean isGamePlayer1(int player1, int player2)
        {
            return ((player1 == 5) && (player2 < player1));
        }

        private static Boolean isGamePlayer2(int player1, int player2)
        {
            return ((player1 < player2) && (player2 == 5));
        }

        private static string GetValuePoint(int point)
        {
            switch (point)
            {
                case -1: return "Deuce";    // empate
                case  0: return "Love";      // 0
                case  1: return "Fifteen";   // 15
                case  2: return "Thirty";    // 30
                case  3: return "Forty";     // 40               
                case  4: return "Advantage"; // ventaja
                case  5: return "Game";      // juego
                default: return "";
            }
        }
    }
}
