﻿# Friendly Rentals Tennis kata #
  
Tennis kata for Friendly rentals  
  
### Set up ###
  
Fork this repository and add "_" and your name as a suffix  
  
Use a TDD aproach.  
Do a commit after each test, implementation, and refactor you do.   
Time between commits will be taken into account so hurry up!    
 
Share the final result with me  
:-)  
  
### Kata description ###
  
The goal was to implement a mechanism, which prints the score of a single game (therefore there are no sets)..   
Make sure you only test for correct inputs. there is no need to test for invalid inputs for this kata  
  
The rules are the following:  
  
* A game is won by the first player to have won at least four points in total and at least two points more than the opponent.  
* The running score of each game is described in a manner peculiar to tennis: scores from zero to three points are described as “love”, “fifteen”, “thirty”, and “forty” respectively.  
* If at least three points have been scored by each player, and the scores are equal, the score is “deuce”.  
* If at least three points have been scored by each side and a player has one more point than his opponent, the score of the game is “advantage” + "name of the player" for the player in the lead.  
